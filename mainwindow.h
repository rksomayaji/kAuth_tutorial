#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QVBoxLayout>
#include <QString>
#include <QLabel>
#include <KAuth>
#include <KAuthAction>
#include <QFile>
#include <QDir>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public slots:
    void display_text();
private:
    Ui::MainWindow *ui;
    QVBoxLayout *main_layout;
    QLabel *display;
    QPushButton *display_text_button;
};

#endif // MAINWINDOW_H
