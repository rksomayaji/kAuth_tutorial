#include "mainwindow.h"
#include "ui_mainwindow.h"

QString display_static_text(){
    return "This is a test";
}

QString display_from_file(){
    QString path = QDir::homePath() + "/fun";
    QString text;
    QFile file(path);
    KAuth::Action action = KAuth::Action("org.kde.auth.trial.read");
    KAuth::ExecuteJob *job = action.execute();
    bool success = job->exec();

    if(success){
        if(file.open(QIODevice::ReadOnly)){
            text = file.readAll();
        }else{
            text = file.errorString() + " " + path;
        }
    }else{
        text = job->errorString();
    }

    return text;
}

void MainWindow::display_text(){
    display->setText(display_from_file());
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QString text = display_static_text();

    display_text_button = new QPushButton("Show Text");

    connect(display_text_button,SIGNAL(clicked(bool)),this,SLOT(display_text()));

    display = new QLabel;
    display->setText(text);

    main_layout = ui->mainLayout;
    main_layout->addWidget(display);
    main_layout->addWidget(display_text_button);
}

MainWindow::~MainWindow()
{
    delete ui;
}
